import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as simplewebauthn from '@simplewebauthn/server';
import * as simplewebauthnHelpers from '@simplewebauthn/server/helpers';
import {
  GenerateAuthenticationOptionsReqBody,
  GenerateRegistrationOptionsConfig,
  RegistrationVerificationConfig,
  VerificationReqBody,
} from 'src/models/webauthn';
import { CREDENTIAL_OPTIONS, challenge } from 'src/utils/constants';
import { WebAuthn } from 'src/utils/webauthn';
import { User } from 'src/user/user.entity';
import { Authenticator } from 'src/authenticators/authenticator.entity';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private usersRepo: Repository<User>,

    @InjectRepository(Authenticator)
    private authenticatorRepo: Repository<Authenticator>
  ) {}

  async generateRegistrationOptions(config: GenerateRegistrationOptionsConfig) {
    const { reqBody } = config;

    const user = await this.usersRepo.findOneBy({ email: reqBody.userEmail });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    const allUserAuthenticators = await this.authenticatorRepo.findBy({
      user: user,
    });

    const generatedOptions = simplewebauthn.generateRegistrationOptions({
      rpName: CREDENTIAL_OPTIONS.rpName,
      challenge: WebAuthn.transformUint8ArrayToBase64String(challenge),
      rpID: CREDENTIAL_OPTIONS.rpID,
      userDisplayName: reqBody.userName,
      userID: reqBody.userEmail,
      userName: reqBody.userEmail,
      attestationType:
        CREDENTIAL_OPTIONS.attestationType as AttestationConveyancePreference,

      excludeCredentials: (allUserAuthenticators as any[]).map(
        curAuthenticator => ({
          id: curAuthenticator.credentialID,
          type: 'public-key',
        })
      ),
    });

    user.challenge = generatedOptions.challenge;
    const savedUser = await this.usersRepo.save(user);

    return {
      generatedOptions,
      user: savedUser,
    };
  }

  async registrationVerification(config: RegistrationVerificationConfig) {
    const { reqBody } = config;
    const user = await this.usersRepo.findOneBy({ id: +reqBody.user.id });

    const verificationJSON = await simplewebauthn.verifyRegistrationResponse({
      response: reqBody.credentials,
      expectedChallenge: user.challenge,
      expectedRPID: CREDENTIAL_OPTIONS.rpID,
      expectedOrigin: CREDENTIAL_OPTIONS.origin,
    });

    if (verificationJSON.verified && verificationJSON?.registrationInfo) {
      const { credentialID, credentialPublicKey, counter } =
        verificationJSON.registrationInfo;

      const newAuthenticator = {
        credentialID:
          simplewebauthnHelpers.isoBase64URL.fromBuffer(credentialID),

        credentialPublicKey:
          simplewebauthnHelpers.isoBase64URL.fromBuffer(credentialPublicKey),

        counter: counter,
      };

      const newAuthenticatorEntity = this.authenticatorRepo.create({
        ...newAuthenticator,
        user,
      });

      await this.authenticatorRepo.save(newAuthenticatorEntity);
      await this.usersRepo.save(user);

      const userAuthenticators = await this.authenticatorRepo.findBy({ user });

      const userWithAuthenticators = {
        ...user,
        authenticators: userAuthenticators,
      };

      return {
        verified: verificationJSON.verified,
        user: userWithAuthenticators,
      };
    }

    return {
      verified: verificationJSON.verified,
      user,
    };
  }

  async generateAuthenticationOptions(
    reqBody: GenerateAuthenticationOptionsReqBody
  ) {
    const user = await this.usersRepo.findOneBy({ email: reqBody.email });
    const userAuthenticators = await this.authenticatorRepo.findBy({ user });

    if (!user) throw new BadRequestException('User does not exist');

    const options = simplewebauthn.generateAuthenticationOptions({
      allowCredentials: userAuthenticators.map(curAuthenticator => ({
        id: simplewebauthnHelpers.isoBase64URL.toBuffer(
          curAuthenticator.credentialID
        ),
        type: 'public-key',
      })),

      userVerification: 'preferred',
    });

    user.loginChallenge = options.challenge;
    await this.usersRepo.save(user);

    return {
      authOptions: options,
      user: {
        name: user.userName,
        _id: user.id,
      },
    };
  }

  async verifyAuthenticationResponse(reqBody: VerificationReqBody) {
    const user = await this.usersRepo.findOneBy({ id: +reqBody.user._id });
    const userAuthenticators = await this.authenticatorRepo.findBy({ user });

    if (!user) {
      throw new BadRequestException('User not found');
    }

    const authenticator = WebAuthn.getAuthenticatorData(
      userAuthenticators,
      reqBody.credentials.id
    );

    if (!authenticator) {
      throw new BadRequestException('Authenticator not found');
    }

    const { verified } = await simplewebauthn.verifyAuthenticationResponse({
      response: reqBody.credentials,
      expectedChallenge: user.loginChallenge,
      expectedRPID: CREDENTIAL_OPTIONS.rpID,
      expectedOrigin: CREDENTIAL_OPTIONS.origin,
      authenticator: authenticator as any,
    });

    const newUser = {
      ...user,
      authenticators: userAuthenticators,
    };

    return { verified, user: newUser };
  }
}
