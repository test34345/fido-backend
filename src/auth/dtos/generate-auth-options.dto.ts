import { IsString, IsEmail } from 'class-validator';

export class GenerateAuthOptionsDto {
  @IsEmail()
  email: string;
}
