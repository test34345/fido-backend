import { IsString, IsEmail } from 'class-validator';

export class GenerateRegistrationOptionsDto {
  @IsString()
  userName: string;

  @IsEmail()
  userEmail: string;
}
