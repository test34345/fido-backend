import { Body, Controller, Delete, Param, Post } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserReqBody, LoginUserReqBody } from './user.models';
import { CreateUserDto } from './dtos/create-user.dto';
import { LoginUserDto } from './dtos/login-user.dto';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('create')
  createUser(@Body() reqBody: CreateUserDto) {
    return this.userService.createUser({ reqBody });
  }

  @Post('login')
  loginUser(@Body() reqBody: LoginUserDto) {
    return this.userService.loginUser({ reqBody });
  }

  @Delete(':id')
  deleteUser(@Param('id') userId: string) {
    return this.userService.deleteUser(userId);
  }
}
