import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserConfig, LoginUserConfig } from './user.models';
import { User } from './user.entity';
import { Authenticator } from 'src/authenticators/authenticator.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepo: Repository<User>,

    @InjectRepository(Authenticator)
    private authenticatorsRepo: Repository<Authenticator>
  ) {}

  async createUser(config: CreateUserConfig) {
    const { reqBody } = config;

    const userExists = await this.usersRepo.exist({
      where: { email: reqBody.email },
    });

    if (userExists)
      throw new BadRequestException('User with this email already exists');

    const user = this.usersRepo.create({
      email: reqBody.email,
      password: reqBody.password,
      userName: reqBody.name,
      challenge: 'initial_challenge',
      loginChallenge: 'initial_challenge',
      authenticators: [],
    });

    await this.usersRepo.save(user);

    return {
      user,
    };
  }

  async loginUser(config: LoginUserConfig) {
    const { reqBody } = config;

    const user = await this.usersRepo.findOneBy({ email: reqBody.email });

    if (!user)
      throw new NotFoundException('User with these details does not exist');

    // Get the authenticators for the user
    const authenticators = await this.authenticatorsRepo.findBy({ user });

    const userWithAuthenticators = {
      ...user,
      authenticators,
    };

    return {
      user: userWithAuthenticators,
    };
  }

  async deleteUser(userId: string) {
    const user = await this.usersRepo.findOneBy({ id: +userId });

    if (!user) throw new NotFoundException('User not found');

    const allUserAuthenticators = await this.authenticatorsRepo.findBy({
      user,
    });

    await this.authenticatorsRepo.remove(allUserAuthenticators);
    await this.usersRepo.remove(user);

    return {
      status: 'success',
      message: 'User deleted',
    };
  }
}
