export type CredentialsResponse = {
  attestationObject: string;
  clientDataJSON: string;
  transports: any[];
};

export type GenerateRegistrationOptionsReqBody = {
  userName: string;
  userEmail: string;
};

export type UserVerificationData = {
  _id: string;
  challenge: string;
};

export type VerificationReqBody = {
  user: UserVerificationData & { id: string };
  credentials: any;
};

export type RegistrationHandlerConfig<T> = {
  reqBody: T;
};

export type GenerateRegistrationOptionsConfig =
  RegistrationHandlerConfig<GenerateRegistrationOptionsReqBody>;

export type RegistrationVerificationConfig =
  RegistrationHandlerConfig<VerificationReqBody>;

export type LoginVerificationConfig =
  RegistrationHandlerConfig<VerificationReqBody>;

export type GenerateAuthenticationOptionsReqBody = {
  email: string;
};
