export class Base64Utils {
  static u8IntArrayToBase64(arr: Uint8Array) {
    const decoder = new TextDecoder('utf8');
    const b64encoded = btoa(decoder.decode(arr));
    return b64encoded;
  }
}
