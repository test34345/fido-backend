import * as simplewebauthnHelpers from '@simplewebauthn/server/helpers';
import { Authenticator, AuthenticatorDecoded } from 'src/user/user.models';

export class WebAuthn {
  static transformUint8ArrayToBase64String(value: Uint8Array) {
    const decoder = new TextDecoder('utf8');
    const b64encoded = btoa(decoder.decode(value));

    return b64encoded;
  }

  static stringToBuffer(value: string) {
    return simplewebauthnHelpers.isoBase64URL.toBuffer(value);
  }

  static bufferToBase64(value: Uint8Array) {
    return simplewebauthnHelpers.isoBase64URL.fromBuffer(value);
  }

  static decodeAuthenticator(authenticator: Authenticator) {
    return {
      credentialID: WebAuthn.stringToBuffer(authenticator.credentialID),
      credentialPublicKey: WebAuthn.stringToBuffer(
        authenticator.credentialPublicKey
      ),
      counter: authenticator.counter,
    };
  }

  static getAuthenticatorData(
    userAuthenticators: Authenticator[],
    authenticatorId: string
  ): AuthenticatorDecoded | null {
    const authenticator = userAuthenticators.find(
      curAuthenticator => curAuthenticator.credentialID === authenticatorId
    );

    if (!authenticator) return null;

    return WebAuthn.decodeAuthenticator(authenticator);
  }

  static isAuthenticatorAlreadyRegistered(
    userAuthenticators: Authenticator[],
    authenticatorId: string
  ) {
    return userAuthenticators.some(
      curAuthenticator =>
        curAuthenticator.credentialPublicKey === authenticatorId
    );
  }
}
