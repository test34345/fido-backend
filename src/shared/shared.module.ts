import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Authenticator } from 'src/authenticators/authenticator.entity';
import { User } from 'src/user/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Authenticator])],
  exports: [TypeOrmModule],
})
export class SharedModule {}
